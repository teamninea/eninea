<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230609123305 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE nin_personne CHANGE nin_qvh_id nin_qvh_id VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE qvh ADD nin_cacr_id VARCHAR(25) DEFAULT NULL, CHANGE id id VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE qvh ADD CONSTRAINT FK_907EEC6FA3226B1B FOREIGN KEY (nin_cacr_id) REFERENCES ref_cacr (id)');
        $this->addSql('CREATE INDEX IDX_907EEC6FA3226B1B ON qvh (nin_cacr_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE nin_personne CHANGE nin_qvh_id nin_qvh_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE qvh DROP FOREIGN KEY FK_907EEC6FA3226B1B');
        $this->addSql('DROP INDEX IDX_907EEC6FA3226B1B ON qvh');
        $this->addSql('ALTER TABLE qvh DROP nin_cacr_id, CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
