<?php

namespace App\Repository;

use App\Entity\NinPartage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NinPartage>
 *
 * @method NinPartage|null find($id, $lockMode = null, $lockVersion = null)
 * @method NinPartage|null findOneBy(array $criteria, array $orderBy = null)
 * @method NinPartage[]    findAll()
 * @method NinPartage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NinPartageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NinPartage::class);
    }

    public function add(NinPartage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(NinPartage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }



    public function findBySearchTerms($terms="")
    {

        $qb= $this->createQueryBuilder('np')
        ;
        
        if ($terms) {
            $qb = $qb->andWhere('np.ninNinea LIKE :terms')
                     ->orWhere('np.nom LIKE :terms')
                     ->orWhere('np.prenom LIKE :terms')
                     ->orWhere('np.ninREGIMEJURIDIQUE LIKE :terms')
                     ->orWhere('np.ninENSEIGNE LIKE :terms')
                     ->orWhere('np.ninRAISONSOCIALE LIKE :terms')
                     ->orWhere('np.ninCNI LIKE :terms')
                     ->orWhere('np.ninNOMCOMMERCIAL LIKE :terms')
                     ->orWhere('np.ninFORMEUNITE LIKE :terms')
                     ->orWhere('np.ninTYPEDEDOCUMENT LIKE :terms')
                     ->orWhere('np.ninNUMERODEDOCUMENT LIKE :terms')
                     ->orWhere('np.ninCIVILITE LIKE :terms')
                     ->orWhere('np.ninSIGLE LIKE :terms')
                     ->orWhere('np.ninLOCALITE LIKE :terms')
                     ->orWhere('np.ninVILLE LIKE :terms')
                     ->orWhere('np.ninNATIONALITE LIKE :terms')
                     ->setParameter('terms', '%'.$terms.'%')
            ;

            
            $query = $qb  
            ->setMaxResults(1000)
            ->getQuery()
            ->getResult();

            return $query;
            
            //return $qb->orderBy('np.createdAt', 'DESC')
            //          ->getQuery();
            // return $qb->getQuery()->getResult();
        }

        return $qb->orderBy('np.createdAt', 'DESC')
                  ->getQuery();
        //return $qb->getQuery()->getResult();

    }

//    /**
//     * @return NinPartage[] Returns an array of NinPartage objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('n.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?NinPartage
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
