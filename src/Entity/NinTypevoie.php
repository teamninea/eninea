<?php

namespace App\Entity;

use App\Repository\NinTypevoieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinTypevoieRepository::class)
 */
class NinTypevoie
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=NinPersonne::class, mappedBy="ninTypevoie")
     */
    private $ninPersonnes;

    /**
     * @ORM\OneToMany(targetEntity=NinCoordonnees::class, mappedBy="ninTypevoie")
     */
    private $ninCoordonnees;

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->ninPersonnes = new ArrayCollection();
        $this->ninCoordonnees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, NinPersonne>
     */
    public function getNinPersonnes(): Collection
    {
        return $this->ninPersonnes;
    }

    public function addNinPersonne(NinPersonne $ninPersonne): self
    {
        if (!$this->ninPersonnes->contains($ninPersonne)) {
            $this->ninPersonnes[] = $ninPersonne;
            $ninPersonne->setNinTypevoie($this);
        }

        return $this;
    }

    public function removeNinPersonne(NinPersonne $ninPersonne): self
    {
        if ($this->ninPersonnes->removeElement($ninPersonne)) {
            // set the owning side to null (unless already changed)
            if ($ninPersonne->getNinTypevoie() === $this) {
                $ninPersonne->setNinTypevoie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NinCoordonnees>
     */
    public function getNinCoordonnees(): Collection
    {
        return $this->ninCoordonnees;
    }

    public function addNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if (!$this->ninCoordonnees->contains($ninCoordonnee)) {
            $this->ninCoordonnees[] = $ninCoordonnee;
            $ninCoordonnee->setNinTypevoie($this);
        }

        return $this;
    }

    public function removeNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if ($this->ninCoordonnees->removeElement($ninCoordonnee)) {
            // set the owning side to null (unless already changed)
            if ($ninCoordonnee->getNinTypevoie() === $this) {
                $ninCoordonnee->setNinTypevoie(null);
            }
        }

        return $this;
    }
}
