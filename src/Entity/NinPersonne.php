<?php

namespace App\Entity;

use App\Repository\NinPersonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinPersonneRepository::class)
 */
class NinPersonne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ninRaisonsociale;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninSigle;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninNom;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninPrenom;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninPersonne")
     */
    private $nineaProposition;

    /**
     * @ORM\ManyToOne(targetEntity=NinSexe::class, inversedBy="ninPersonnes")
     */
    private $ninSexe;

    /**
     * @ORM\ManyToOne(targetEntity=NinCivilite::class, inversedBy="ninPersonnes")
     */
    private $ninCivilite;

    /**
     * @ORM\ManyToOne(targetEntity=NinPays::class, inversedBy="ninPersonnes")
     */
    private $ninNationalite;

    /**
     * @ORM\ManyToOne(targetEntity=NinTypevoie::class, inversedBy="ninPersonnes")
     */
    private $ninTypevoie;

    /**
     * @ORM\ManyToOne(targetEntity=QVH::class, inversedBy="ninPersonnes")
     */
    private $ninQVH;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ninDatenaissance;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $ninLieunaissance;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $ninCni;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ninDatecni;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninTelephone;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninAdresse;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ninVoie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ninNumerovoie;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $createdAt;

    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function __construct()
    {
        $this->nineaProposition = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNinRaisonsociale(): ?string
    {
        return $this->ninRaisonsociale;
    }

    public function setNinRaisonsociale(?string $ninRaisonsociale): self
    {
        $this->ninRaisonsociale = $ninRaisonsociale;

        return $this;
    }

    public function getNinSigle(): ?string
    {
        return $this->ninSigle;
    }

    public function setNinSigle(?string $ninSigle): self
    {
        $this->ninSigle = $ninSigle;

        return $this;
    }

    public function getNinNom(): ?string
    {
        return $this->ninNom;
    }

    public function setNinNom(?string $ninNom): self
    {
        $this->ninNom = $ninNom;

        return $this;
    }

    public function getNinPrenom(): ?string
    {
        return $this->ninPrenom;
    }

    public function setNinPrenom(?string $ninPrenom): self
    {
        $this->ninPrenom = $ninPrenom;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaProposition(): Collection
    {
        return $this->nineaProposition;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaProposition->contains($nineaProposition)) {
            $this->nineaProposition[] = $nineaProposition;
            $nineaProposition->setNinPersonne($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaProposition->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinPersonne() === $this) {
                $nineaProposition->setNinPersonne(null);
            }
        }

        return $this;
    }

    public function getNinSexe(): ?NinSexe
    {
        return $this->ninSexe;
    }

    public function setNinSexe(?NinSexe $ninSexe): self
    {
        $this->ninSexe = $ninSexe;

        return $this;
    }

    public function getNinCivilite(): ?NinCivilite
    {
        return $this->ninCivilite;
    }

    public function setNinCivilite(?NinCivilite $ninCivilite): self
    {
        $this->ninCivilite = $ninCivilite;

        return $this;
    }

    public function getNinNationalite(): ?NinPays
    {
        return $this->ninNationalite;
    }

    public function setNinNationalite(?NinPays $ninNationalite): self
    {
        $this->ninNationalite = $ninNationalite;

        return $this;
    }

    public function getNinTypevoie(): ?NinTypevoie
    {
        return $this->ninTypevoie;
    }

    public function setNinTypevoie(?NinTypevoie $ninTypevoie): self
    {
        $this->ninTypevoie = $ninTypevoie;

        return $this;
    }

    public function getNinQVH(): ?QVH
    {
        return $this->ninQVH;
    }

    public function setNinQVH(?QVH $ninQVH): self
    {
        $this->ninQVH = $ninQVH;

        return $this;
    }

    public function getNinDatenaissance(): ?\DateTimeInterface
    {
        return $this->ninDatenaissance;
    }

    public function setNinDatenaissance(?\DateTimeInterface $ninDatenaissance): self
    {
        $this->ninDatenaissance = $ninDatenaissance;

        return $this;
    }

    public function getNinLieunaissance(): ?string
    {
        return $this->ninLieunaissance;
    }

    public function setNinLieunaissance(string $ninLieunaissance): self
    {
        $this->ninLieunaissance = $ninLieunaissance;

        return $this;
    }

    public function getNinCni(): ?string
    {
        return $this->ninCni;
    }

    public function setNinCni(?string $ninCni): self
    {
        $this->ninCni = $ninCni;

        return $this;
    }

    public function getNinDatecni(): ?\DateTimeInterface
    {
        return $this->ninDatecni;
    }

    public function setNinDatecni(?\DateTimeInterface $ninDatecni): self
    {
        $this->ninDatecni = $ninDatecni;

        return $this;
    }

    public function getNinTelephone(): ?string
    {
        return $this->ninTelephone;
    }

    public function setNinTelephone(?string $ninTelephone): self
    {
        $this->ninTelephone = $ninTelephone;

        return $this;
    }

    public function getNinEmail(): ?string
    {
        return $this->ninEmail;
    }

    public function setNinEmail(?string $ninEmail): self
    {
        $this->ninEmail = $ninEmail;

        return $this;
    }

    public function getNinAdresse(): ?string
    {
        return $this->ninAdresse;
    }

    public function setNinAdresse(?string $ninAdresse): self
    {
        $this->ninAdresse = $ninAdresse;

        return $this;
    }

    public function getNinVoie(): ?string
    {
        return $this->ninVoie;
    }

    public function setNinVoie(?string $ninVoie): self
    {
        $this->ninVoie = $ninVoie;

        return $this;
    }

    public function getNinNumerovoie(): ?string
    {
        return $this->ninNumerovoie;
    }

    public function setNinNumerovoie(?string $ninNumerovoie): self
    {
        $this->ninNumerovoie = $ninNumerovoie;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
