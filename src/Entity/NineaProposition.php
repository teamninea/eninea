<?php

namespace App\Entity;

use App\Repository\NineaPropositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NineaPropositionRepository::class)
 */
class NineaProposition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $ninNinea;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninSigle;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $ninEnseigne;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $ninNomcommercial;

    /**
     * @ORM\OneToMany(targetEntity=Piecesjointes::class, mappedBy="nineaProposition")
     */
    private $piecesJointes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ninEtat;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ninMajdate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ninActiviteglobale;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $ninNumerodocument;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ninDatedocument;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=NinPersonne::class, inversedBy="nineaProposition", cascade={"persist"})
     */
    private $ninPersonne;

    /**
     * @ORM\ManyToOne(targetEntity=NinTypedocuments::class, inversedBy="nineaPropositions")
     */
    private $ninTypedocuments;

    /**
     * @ORM\ManyToOne(targetEntity=NinStatut::class, inversedBy="nineaPropositions")
     */
    private $ninStatut;


    /**
     * @ORM\ManyToOne(targetEntity=NiFormejuridique::class, inversedBy="nineaPropositions")
     */
    private $formeJuridique;

    /**
     * @ORM\OneToMany(targetEntity=NinCoordonnees::class, mappedBy="nineaProposition", cascade={"persist"},orphanRemoval=true)
     */
    private $ninCoordonnees;

    /**
     * @ORM\ManyToOne(targetEntity=RefRegion::class, inversedBy="nineaPropositions")
     */
    private $ninRegion;

    /**
     * @ORM\ManyToOne(targetEntity=RefDepartement::class, inversedBy="nineaPropositions")
     */
    private $ninDepartement;

    /**
     * @ORM\ManyToOne(targetEntity=RefCAV::class, inversedBy="nineaPropositions")
     */
    private $ninCav;

    /**
     * @ORM\ManyToOne(targetEntity=RefCACR::class, inversedBy="nineaPropositions")
     */
    private $ninCacr;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $cniFileName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $rccmFileName;

    public function __construct()
    {
        $this->piecesJointes = new ArrayCollection();
        $this->ninCoordonnees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNinNinea(): ?string
    {
        return $this->ninNinea;
    }

    public function setNinNinea(?string $ninNinea): self
    {
        $this->ninNinea = $ninNinea;

        return $this;
    }

    public function getNinSigle(): ?string
    {
        return $this->ninSigle;
    }

    public function setNinSigle(?string $ninSigle): self
    {
        $this->ninSigle = $ninSigle;

        return $this;
    }

    public function getNinEnseigne(): ?string
    {
        return $this->ninEnseigne;
    }

    public function setNinEnseigne(?string $ninEnseigne): self
    {
        $this->ninEnseigne = $ninEnseigne;

        return $this;
    }

    public function getNinNomcommercial(): ?string
    {
        return $this->ninNomcommercial;
    }

    public function setNinNomcommercial(?string $ninNomcommercial): self
    {
        $this->ninNomcommercial = $ninNomcommercial;

        return $this;
    }

    /**
     * @return Collection<int, Piecesjointes>
     */
    public function getPiecesJointes(): Collection
    {
        return $this->piecesJointes;
    }

    public function addPiecesJointe(Piecesjointes $piecesJointe): self
    {
        if (!$this->piecesJointes->contains($piecesJointe)) {
            $this->piecesJointes[] = $piecesJointe;
            $piecesJointe->setNineaProposition($this);
        }

        return $this;
    }

    public function removePiecesJointe(Piecesjointes $piecesJointe): self
    {
        if ($this->piecesJointes->removeElement($piecesJointe)) {
            // set the owning side to null (unless already changed)
            if ($piecesJointe->getNineaProposition() === $this) {
                $piecesJointe->setNineaProposition(null);
            }
        }

        return $this;
    }

    public function isNinEtat(): ?bool
    {
        return $this->ninEtat;
    }

    public function setNinEtat(?bool $ninEtat): self
    {
        $this->ninEtat = $ninEtat;

        return $this;
    }

    public function getNinMajdate(): ?\DateTimeInterface
    {
        return $this->ninMajdate;
    }

    public function setNinMajdate(?\DateTimeInterface $ninMajdate): self
    {
        $this->ninMajdate = $ninMajdate;

        return $this;
    }

    public function getNinActiviteglobale(): ?string
    {
        return $this->ninActiviteglobale;
    }

    public function setNinActiviteglobale(?string $ninActiviteglobale): self
    {
        $this->ninActiviteglobale = $ninActiviteglobale;

        return $this;
    }

    public function getNinNumerodocument(): ?string
    {
        return $this->ninNumerodocument;
    }

    public function setNinNumerodocument(?string $ninNumerodocument): self
    {
        $this->ninNumerodocument = $ninNumerodocument;

        return $this;
    }

    public function getNinDatedocument(): ?\DateTimeInterface
    {
        return $this->ninDatedocument;
    }

    public function setNinDatedocument(?\DateTimeInterface $ninDatedocument): self
    {
        $this->ninDatedocument = $ninDatedocument;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getNinPersonne(): ?NinPersonne
    {
        return $this->ninPersonne;
    }

    public function setNinPersonne(?NinPersonne $ninPersonne): self
    {
        $this->ninPersonne = $ninPersonne;

        return $this;
    }

    public function getNinTypedocuments(): ?NinTypedocuments
    {
        return $this->ninTypedocuments;
    }

    public function setNinTypedocuments(?NinTypedocuments $ninTypedocuments): self
    {
        $this->ninTypedocuments = $ninTypedocuments;

        return $this;
    }

    public function getNinStatut(): ?NinStatut
    {
        return $this->ninStatut;
    }

    public function setNinStatut(?NinStatut $ninStatut): self
    {
        $this->ninStatut = $ninStatut;

        return $this;
    }


    public function getFormeJuridique(): ?NiFormejuridique
    {
        return $this->formeJuridique;
    }

    public function setFormeJuridique(?NiFormejuridique $formeJuridique): self
    {
        $this->formeJuridique = $formeJuridique;

        return $this;
    }

    /**
     * @return Collection<int, NinCoordonnees>
     */
    public function getNinCoordonnees(): Collection
    {
        return $this->ninCoordonnees;
    }

    public function addNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if (!$this->ninCoordonnees->contains($ninCoordonnee)) {
            $this->ninCoordonnees[] = $ninCoordonnee;
            $ninCoordonnee->setNineaProposition($this);
        }

        return $this;
    }

    public function removeNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if ($this->ninCoordonnees->removeElement($ninCoordonnee)) {
            // set the owning side to null (unless already changed)
            if ($ninCoordonnee->getNineaProposition() === $this) {
                $ninCoordonnee->setNineaProposition(null);
            }
        }

        return $this;
    }

    public function getNinRegion(): ?RefRegion
    {
        return $this->ninRegion;
    }

    public function setNinRegion(?RefRegion $ninRegion): self
    {
        $this->ninRegion = $ninRegion;

        return $this;
    }

    public function getNinDepartement(): ?RefDepartement
    {
        return $this->ninDepartement;
    }

    public function setNinDepartement(?RefDepartement $ninDepartement): self
    {
        $this->ninDepartement = $ninDepartement;

        return $this;
    }

    public function getNinCav(): ?RefCAV
    {
        return $this->ninCav;
    }

    public function setNinCav(?RefCAV $ninCav): self
    {
        $this->ninCav = $ninCav;

        return $this;
    }

    public function getNinCacr(): ?RefCACR
    {
        return $this->ninCacr;
    }

    public function setNinCacr(?RefCACR $ninCacr): self
    {
        $this->ninCacr = $ninCacr;

        return $this;
    }

    public function getCniFileName(): ?string
    {
        return $this->cniFileName;
    }

    public function setCniFileName(?string $cniFileName): self
    {
        $this->cniFileName = $cniFileName;

        return $this;
    }

    public function getRccmFileName(): ?string
    {
        return $this->rccmFileName;
    }

    public function setRccmFileName(?string $rccmFileName): self
    {
        $this->rccmFileName = $rccmFileName;

        return $this;
    }
}
