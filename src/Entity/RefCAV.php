<?php

namespace App\Entity;

use App\Repository\RefCAVRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RefCAVRepository::class)
 */
class RefCAV
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $codeCav;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity=RefDepartement::class, inversedBy="refCAVs")
     */
    private $ninDepartement;

    
    /**
     * @ORM\OneToMany(targetEntity=RefCACR::class, mappedBy="ninCAV")
     */
    private $refCACRs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $cavActiveF;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cavCDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cavCUSER;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $cavCDMIG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cavMDATE;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $cavMUSER;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninCav")
     */
    private $nineaPropositions;

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->refCACRs = new ArrayCollection();
        $this->nineaPropositions = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNinDepartement(): ?RefDepartement
    {
        return $this->ninDepartement;
    }

    public function setNinDepartement(?RefDepartement $ninDepartement): self
    {
        $this->ninDepartement = $ninDepartement;

        return $this;
    }

    /**
     * @return Collection<int, RefCACR>
     */
    public function getRefCACRs(): Collection
    {
        return $this->refCACRs;
    }

    public function addRefCACR(RefCACR $refCACR): self
    {
        if (!$this->refCACRs->contains($refCACR)) {
            $this->refCACRs[] = $refCACR;
            $refCACR->setNinCAV($this);
        }

        return $this;
    }

    public function removeRefCACR(RefCACR $refCACR): self
    {
        if ($this->refCACRs->removeElement($refCACR)) {
            // set the owning side to null (unless already changed)
            if ($refCACR->getNinCAV() === $this) {
                $refCACR->setNinCAV(null);
            }
        }

        return $this;
    }

    public function getCodeCav(): ?string
    {
        return $this->codeCav;
    }

    public function setCodeCav(?string $codeCav): self
    {
        $this->codeCav = $codeCav;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCavActiveF(): ?string
    {
        return $this->cavActiveF;
    }

    public function setCavActiveF(?string $cavActiveF): self
    {
        $this->cavActiveF = $cavActiveF;

        return $this;
    }

    public function getCavCDATE(): ?string
    {
        return $this->cavCDATE;
    }

    public function setCavCDATE(?string $cavCDATE): self
    {
        $this->cavCDATE = $cavCDATE;

        return $this;
    }

    public function getCavCUSER(): ?string
    {
        return $this->cavCUSER;
    }

    public function setCavCUSER(?string $cavCUSER): self
    {
        $this->cavCUSER = $cavCUSER;

        return $this;
    }

    public function getCavCDMIG(): ?string
    {
        return $this->cavCDMIG;
    }

    public function setCavCDMIG(?string $cavCDMIG): self
    {
        $this->cavCDMIG = $cavCDMIG;

        return $this;
    }

    public function getCavMDATE(): ?string
    {
        return $this->cavMDATE;
    }

    public function setCavMDATE(?string $cavMDATE): self
    {
        $this->cavMDATE = $cavMDATE;

        return $this;
    }

    public function getCavMUSER(): ?string
    {
        return $this->cavMUSER;
    }

    public function setCavMUSER(?string $cavMUSER): self
    {
        $this->cavMUSER = $cavMUSER;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaPropositions(): Collection
    {
        return $this->nineaPropositions;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaPropositions->contains($nineaProposition)) {
            $this->nineaPropositions[] = $nineaProposition;
            $nineaProposition->setNinCav($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaPropositions->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinCav() === $this) {
                $nineaProposition->setNinCav(null);
            }
        }

        return $this;
    }
}
