<?php

namespace App\Entity;

use App\Repository\NinFormeuniteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinFormeuniteRepository::class)
 */
class NinFormeunite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $libelle;


    /**
     * @ORM\OneToMany(targetEntity=NiFormejuridique::class, mappedBy="ninFormeunite")
     */
    private $ninRegimejuridiques;

    public function __construct()
    {
        $this->ninRegimejuridiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


    /**
     * @return Collection<int, NiFormejuridique>
     */
    public function getNinRegimejuridiques(): Collection
    {
        return $this->ninRegimejuridiques;
    }

    public function addNinRegimejuridique(NiFormejuridique $ninRegimejuridiques): self
    {
        if (!$this->ninRegimejuridiques->contains($ninRegimejuridiques)) {
            $this->ninRegimejuridiques[] = $ninRegimejuridiques;
            $ninRegimejuridiques->setNinFormeunite($this);
        }

        return $this;
    }

    public function removeNinRegimejuridiques(NiFormejuridique $ninRegimejuridiques): self
    {
        if ($this->ninRegimejuridiques->removeElement($ninRegimejuridiques)) {
            // set the owning side to null (unless already changed)
            if ($ninRegimejuridiques->getNinFormeunite() === $this) {
                $ninRegimejuridiques->setNinFormeunite(null);
            }
        }

        return $this;
    }

    public function removeNinRegimejuridique(NiFormejuridique $ninRegimejuridique): self
    {
        if ($this->ninRegimejuridiques->removeElement($ninRegimejuridique)) {
            // set the owning side to null (unless already changed)
            if ($ninRegimejuridique->getNinFormeunite() === $this) {
                $ninRegimejuridique->setNinFormeunite(null);
            }
        }

        return $this;
    }
}
