<?php

namespace App\Entity;

use App\Repository\QVHRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QVHRepository::class)
 */
class QVH
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $codeQvh;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle;
    
    /**
     * @ORM\ManyToOne(targetEntity=RefCACR::class, inversedBy="qVHs")
     */
    private $ninCACR;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qvhCUSER;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qvhActiveF;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qvhCDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qvhCDMIG;

    /**
     * @ORM\Column(type="string", length=27, nullable=true)
     */
    private $qvhMDATE;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $qvhUSER;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $qvhType;
    
    /**
     * @ORM\OneToMany(targetEntity=NinPersonne::class, mappedBy="ninQVH")
     */
    private $ninPersonnes;


    /**
     * @ORM\OneToMany(targetEntity=NinCoordonnees::class, mappedBy="ninQvh")
     */
    private $ninCoordonnees;

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->ninPersonnes = new ArrayCollection();
        $this->ninCoordonnees = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCodeQvh(): ?string
    {
        return $this->codeQvh;
    }

    public function setCodeQvh(?string $codeQvh): self
    {
        $this->codeQvh = $codeQvh;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, NinPersonne>
     */
    public function getNinPersonnes(): Collection
    {
        return $this->ninPersonnes;
    }

    public function addNinPersonne(NinPersonne $ninPersonne): self
    {
        if (!$this->ninPersonnes->contains($ninPersonne)) {
            $this->ninPersonnes[] = $ninPersonne;
            $ninPersonne->setNinQVH($this);
        }

        return $this;
    }

    public function removeNinPersonne(NinPersonne $ninPersonne): self
    {
        if ($this->ninPersonnes->removeElement($ninPersonne)) {
            // set the owning side to null (unless already changed)
            if ($ninPersonne->getNinQVH() === $this) {
                $ninPersonne->setNinQVH(null);
            }
        }

        return $this;
    }

    public function getNinCACR(): ?RefCACR
    {
        return $this->ninCACR;
    }

    public function setNinCACR(?RefCACR $ninCACR): self
    {
        $this->ninCACR = $ninCACR;

        return $this;
    }

    /**
     * @return Collection<int, NinCoordonnees>
     */
    public function getNinCoordonnees(): Collection
    {
        return $this->ninCoordonnees;
    }

    public function addNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if (!$this->ninCoordonnees->contains($ninCoordonnee)) {
            $this->ninCoordonnees[] = $ninCoordonnee;
            $ninCoordonnee->setNinQvh($this);
        }

        return $this;
    }

    public function removeNinCoordonnee(NinCoordonnees $ninCoordonnee): self
    {
        if ($this->ninCoordonnees->removeElement($ninCoordonnee)) {
            // set the owning side to null (unless already changed)
            if ($ninCoordonnee->getNinQvh() === $this) {
                $ninCoordonnee->setNinQvh(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQvhCUSER(): ?string
    {
        return $this->qvhCUSER;
    }

    public function setQvhCUSER(?string $qvhCUSER): self
    {
        $this->qvhCUSER = $qvhCUSER;

        return $this;
    }

    public function getQvhActiveF(): ?string
    {
        return $this->qvhActiveF;
    }

    public function setQvhActiveF(?string $qvhActiveF): self
    {
        $this->qvhActiveF = $qvhActiveF;

        return $this;
    }

    public function getQvhCDATE(): ?string
    {
        return $this->qvhCDATE;
    }

    public function setQvhCDATE(?string $qvhCDATE): self
    {
        $this->qvhCDATE = $qvhCDATE;

        return $this;
    }

    public function getQvhCDMIG(): ?string
    {
        return $this->qvhCDMIG;
    }

    public function setQvhCDMIG(?string $qvhCDMIG): self
    {
        $this->qvhCDMIG = $qvhCDMIG;

        return $this;
    }

    public function getQvhMDATE(): ?string
    {
        return $this->qvhMDATE;
    }

    public function setQvhMDATE(?string $qvhMDATE): self
    {
        $this->qvhMDATE = $qvhMDATE;

        return $this;
    }

    public function getQvhUSER(): ?string
    {
        return $this->qvhUSER;
    }

    public function setQvhUSER(?string $qvhUSER): self
    {
        $this->qvhUSER = $qvhUSER;

        return $this;
    }

    public function getQvhType(): ?string
    {
        return $this->qvhType;
    }

    public function setQvhType(?string $qvhType): self
    {
        $this->qvhType = $qvhType;

        return $this;
    }
}
