<?php

namespace App\Entity;

use App\Repository\NinCoordonneesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinCoordonneesRepository::class)
 */
class NinCoordonnees
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=NineaProposition::class, inversedBy="ninCoordonnees", cascade={"persist"})
     */
    private $nineaProposition;

    /**
     * @ORM\ManyToOne(targetEntity=QVH::class, inversedBy="ninCoordonnees")
     */
    private $ninQvh;

    /**
     * @ORM\ManyToOne(targetEntity=NinTypevoie::class, inversedBy="ninCoordonnees")
     */
    private $ninTypevoie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ninNumerovoie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ninVoie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninAdresse1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninEmail;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninTelephone1;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninTelephone2;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ninBp;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninUrl;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $createdAt;


    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNineaProposition(): ?NineaProposition
    {
        return $this->nineaProposition;
    }

    public function setNineaProposition(?NineaProposition $nineaProposition): self
    {
        $this->nineaProposition = $nineaProposition;

        return $this;
    }

    public function getNinQvh(): ?QVH
    {
        return $this->ninQvh;
    }

    public function setNinQvh(?QVH $ninQvh): self
    {
        $this->ninQvh = $ninQvh;

        return $this;
    }

    public function getNinTypevoie(): ?NinTypevoie
    {
        return $this->ninTypevoie;
    }

    public function setNinTypevoie(?NinTypevoie $ninTypevoie): self
    {
        $this->ninTypevoie = $ninTypevoie;

        return $this;
    }

    public function getNinNumerovoie(): ?string
    {
        return $this->ninNumerovoie;
    }

    public function setNinNumerovoie(?string $ninNumerovoie): self
    {
        $this->ninNumerovoie = $ninNumerovoie;

        return $this;
    }

    public function getNinVoie(): ?string
    {
        return $this->ninVoie;
    }

    public function setNinVoie(?string $ninVoie): self
    {
        $this->ninVoie = $ninVoie;

        return $this;
    }

    public function getNinAdresse1(): ?string
    {
        return $this->ninAdresse1;
    }

    public function setNinAdresse1(?string $ninAdresse1): self
    {
        $this->ninAdresse1 = $ninAdresse1;

        return $this;
    }

    public function getNinEmail(): ?string
    {
        return $this->ninEmail;
    }

    public function setNinEmail(?string $ninEmail): self
    {
        $this->ninEmail = $ninEmail;

        return $this;
    }

    public function getNinTelephone1(): ?string
    {
        return $this->ninTelephone1;
    }

    public function setNinTelephone1(?string $ninTelephone1): self
    {
        $this->ninTelephone1 = $ninTelephone1;

        return $this;
    }

    public function getNinTelephone2(): ?string
    {
        return $this->ninTelephone2;
    }

    public function setNinTelephone2(?string $ninTelephone2): self
    {
        $this->ninTelephone2 = $ninTelephone2;

        return $this;
    }

    public function getNinBp(): ?string
    {
        return $this->ninBp;
    }

    public function setNinBp(?string $ninBp): self
    {
        $this->ninBp = $ninBp;

        return $this;
    }

    public function getNinUrl(): ?string
    {
        return $this->ninUrl;
    }

    public function setNinUrl(?string $ninUrl): self
    {
        $this->ninUrl = $ninUrl;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
