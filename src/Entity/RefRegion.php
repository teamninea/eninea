<?php

namespace App\Entity;

use App\Repository\RefRegionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RefRegionRepository::class)
 */
class RefRegion
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=RefDepartement::class, mappedBy="ninRegion")
     */
    private $refDepartements;



    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $regActiveF;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $regCDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regCUSER;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regCDMIG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regContactDetails;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $regMDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regMUSER;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninRegion")
     */
    private $nineaPropositions;
    


    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->refDepartements = new ArrayCollection();
        $this->nineaPropositions = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, RefDepartement>
     */
    public function getRefDepartements(): Collection
    {
        return $this->refDepartements;
    }

    public function addRefDepartement(RefDepartement $refDepartement): self
    {
        if (!$this->refDepartements->contains($refDepartement)) {
            $this->refDepartements[] = $refDepartement;
            $refDepartement->setNinRegion($this);
        }

        return $this;
    }

    public function removeRefDepartement(RefDepartement $refDepartement): self
    {
        if ($this->refDepartements->removeElement($refDepartement)) {
            // set the owning side to null (unless already changed)
            if ($refDepartement->getNinRegion() === $this) {
                $refDepartement->setNinRegion(null);
            }
        }

        return $this;
    }

    public function getRegActiveF(): ?string
    {
        return $this->regActiveF;
    }

    public function setRegActiveF(?string $regActiveF): self
    {
        $this->regActiveF = $regActiveF;

        return $this;
    }

    public function getRegCDATE(): ?string
    {
        return $this->regCDATE;
    }

    public function setRegCDATE(?string $regCDATE): self
    {
        $this->regCDATE = $regCDATE;

        return $this;
    }

    public function getRegCUSER(): ?string
    {
        return $this->regCUSER;
    }

    public function setRegCUSER(?string $regCUSER): self
    {
        $this->regCUSER = $regCUSER;

        return $this;
    }

    public function getRegCDMIG(): ?string
    {
        return $this->regCDMIG;
    }

    public function setRegCDMIG(?string $regCDMIG): self
    {
        $this->regCDMIG = $regCDMIG;

        return $this;
    }

    public function getRegContactDetails(): ?string
    {
        return $this->regContactDetails;
    }

    public function setRegContactDetails(?string $regContactDetails): self
    {
        $this->regContactDetails = $regContactDetails;

        return $this;
    }

    public function getRegMDATE(): ?string
    {
        return $this->regMDATE;
    }

    public function setRegMDATE(?string $regMDATE): self
    {
        $this->regMDATE = $regMDATE;

        return $this;
    }

    public function getRegMUSER(): ?string
    {
        return $this->regMUSER;
    }

    public function setRegMUSER(?string $regMUSER): self
    {
        $this->regMUSER = $regMUSER;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaPropositions(): Collection
    {
        return $this->nineaPropositions;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaPropositions->contains($nineaProposition)) {
            $this->nineaPropositions[] = $nineaProposition;
            $nineaProposition->setNinRegion($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaPropositions->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinRegion() === $this) {
                $nineaProposition->setNinRegion(null);
            }
        }

        return $this;
    }
}
