<?php

namespace App\Entity;

use App\Repository\RefDepartementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RefDepartementRepository::class)
 */
class RefDepartement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $codeDep;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity=RefRegion::class, inversedBy="refDepartements")
     */
    private $ninRegion;

    /**
     * @ORM\OneToMany(targetEntity=RefCAV::class, mappedBy="ninDepartement")
     */
    private $refCAVs;


    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $depActiveF;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depCDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depCUSER;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depCDMIG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depMDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $depMUSER;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninDepartement")
     */
    private $nineaPropositions;    


    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->refCAVs = new ArrayCollection();
        $this->nineaPropositions = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNinRegion(): ?RefRegion
    {
        return $this->ninRegion;
    }

    public function setNinRegion(?RefRegion $ninRegion): self
    {
        $this->ninRegion = $ninRegion;

        return $this;
    }

    /**
     * @return Collection<int, RefCAV>
     */
    public function getRefCAVs(): Collection
    {
        return $this->refCAVs;
    }

    public function addRefCAV(RefCAV $refCAV): self
    {
        if (!$this->refCAVs->contains($refCAV)) {
            $this->refCAVs[] = $refCAV;
            $refCAV->setNinDepartement($this);
        }

        return $this;
    }

    public function removeRefCAV(RefCAV $refCAV): self
    {
        if ($this->refCAVs->removeElement($refCAV)) {
            // set the owning side to null (unless already changed)
            if ($refCAV->getNinDepartement() === $this) {
                $refCAV->setNinDepartement(null);
            }
        }

        return $this;
    }

    public function getCodeDep(): ?string
    {
        return $this->codeDep;
    }

    public function setCodeDep(?string $codeDep): self
    {
        $this->codeDep = $codeDep;

        return $this;
    }

    public function getDepActiveF(): ?string
    {
        return $this->depActiveF;
    }

    public function setDepActiveF(?string $depActiveF): self
    {
        $this->depActiveF = $depActiveF;

        return $this;
    }

    public function getDepCDATE(): ?string
    {
        return $this->depCDATE;
    }

    public function setDepCDATE(?string $depCDATE): self
    {
        $this->depCDATE = $depCDATE;

        return $this;
    }

    public function getDepCUSER(): ?string
    {
        return $this->depCUSER;
    }

    public function setDepCUSER(?string $depCUSER): self
    {
        $this->depCUSER = $depCUSER;

        return $this;
    }

    public function getDepCDMIG(): ?string
    {
        return $this->depCDMIG;
    }

    public function setDepCDMIG(?string $depCDMIG): self
    {
        $this->depCDMIG = $depCDMIG;

        return $this;
    }

    public function getDepMDATE(): ?string
    {
        return $this->depMDATE;
    }

    public function setDepMDATE(?string $depMDATE): self
    {
        $this->depMDATE = $depMDATE;

        return $this;
    }

    public function getDepMUSER(): ?string
    {
        return $this->depMUSER;
    }

    public function setDepMUSER(?string $depMUSER): self
    {
        $this->depMUSER = $depMUSER;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaPropositions(): Collection
    {
        return $this->nineaPropositions;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaPropositions->contains($nineaProposition)) {
            $this->nineaPropositions[] = $nineaProposition;
            $nineaProposition->setNinDepartement($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaPropositions->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinDepartement() === $this) {
                $nineaProposition->setNinDepartement(null);
            }
        }

        return $this;
    }
}
