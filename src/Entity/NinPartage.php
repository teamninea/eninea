<?php

namespace App\Entity;

use App\Repository\NinPartageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinPartageRepository::class)
 * @ORM\Table(name="`nin_partage`")
 */
class NinPartage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nin_ninea", type="string", length=255, nullable=true)
     */
    private $ninNinea;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninREGIMEJURIDIQUE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNATIONALITE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninENSEIGNE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninDATECNI;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninRAISONSOCIALE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninCNI;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninOPERATION;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNUMCNI;

      /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ninLIBELLEACTIVITEGLOBALE;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNOMCOMMERCIAL;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninFORMEUNITE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninSTATUT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninTYPEDEDOCUMENT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNUMERODEDOCUMENT;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ninDATEDUDOCUMENT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninCIVILITE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninSEXE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninDATEDENAISSANCE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninLIEUDENAISSANCE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninEMAILENTREPRISE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninSIGLE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninTYPEVOIE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNUMEROVOIE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninTELEPHONEENTREPRISE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninCAPITAL;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninEFFECTIFPERMANENT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninEFFECTIFPERMANENTFEMME;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninEFFECTIFTEMPORAIRE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninEFFECTIFTEMPORAIREFEMME;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninCHIFFREAFFAIRE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninSOURCEFINANCEMENT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninMODALITEEXPLOITATION;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninACTIVITE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninPRODUIT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninVOIE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninANNEECA;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ninDATEOPERATION;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ninISTRAITED;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ninDATETRAITEMENT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninLOCALITE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninVILLE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninBOITEPOSTALE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninADRESSE;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninNINEAMERE;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setID(?int $id)
    {
        $this->id = $id;
        return $this->id;
    }

    public function getNinNinea(): ?string
    {
        return $this->ninNinea;
    }

    public function setNinNinea(?string $ninNinea): self
    {
        $this->ninNinea = $ninNinea;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNinREGIMEJURIDIQUE(): ?string
    {
        return $this->ninREGIMEJURIDIQUE;
    }

    public function setNinREGIMEJURIDIQUE(?string $ninREGIMEJURIDIQUE): self
    {
        $this->ninREGIMEJURIDIQUE = $ninREGIMEJURIDIQUE;

        return $this;
    }

    public function getNinNATIONALITE(): ?string
    {
        return $this->ninNATIONALITE;
    }

    public function setNinNATIONALITE(?string $ninNATIONALITE): self
    {
        $this->ninNATIONALITE = $ninNATIONALITE;

        return $this;
    }

    public function getNinENSEIGNE(): ?string
    {
        return $this->ninENSEIGNE;
    }

    public function setNinENSEIGNE(?string $ninENSEIGNE): self
    {
        $this->ninENSEIGNE = $ninENSEIGNE;

        return $this;
    }

    public function getNinDATECNI(): ?string
    {
        return $this->ninDATECNI;
    }

    public function setNinDATECNI(?string $ninDATECNI): self
    {
        $this->ninDATECNI = $ninDATECNI;

        return $this;
    }

    public function getNinRAISONSOCIALE(): ?string
    {
        return $this->ninRAISONSOCIALE;
    }

    public function setNinRAISONSOCIALE(?string $ninRAISONSOCIALE): self
    {
        $this->ninRAISONSOCIALE = $ninRAISONSOCIALE;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNinCNI(): ?string
    {
        return $this->ninCNI;
    }

    public function setNinCNI(?string $ninCNI): self
    {
        $this->ninCNI = $ninCNI;

        return $this;
    }

    public function getNinOPERATION(): ?string
    {
        return $this->ninOPERATION;
    }

    public function setNinOPERATION(?string $ninOPERATION): self
    {
        $this->ninOPERATION = $ninOPERATION;

        return $this;
    }

    public function getNinNUMCNI(): ?string
    {
        return $this->ninNUMCNI;
    }

    public function setNinNUMCNI(?string $ninNUMCNI): self
    {
        $this->ninNUMCNI = $ninNUMCNI;

        return $this;
    }

    public function getNinLIBELLEACTIVITEGLOBALE(): ?string
    {
        return $this->ninLIBELLEACTIVITEGLOBALE;
    }

    public function setNinLIBELLEACTIVITEGLOBALE(?string $ninLIBELLEACTIVITEGLOBALE): self
    {
        $this->ninLIBELLEACTIVITEGLOBALE = $ninLIBELLEACTIVITEGLOBALE;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?string $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getNinNOMCOMMERCIAL(): ?string
    {
        return $this->ninNOMCOMMERCIAL;
    }

    public function setNinNOMCOMMERCIAL(?string $ninNOMCOMMERCIAL): self
    {
        $this->ninNOMCOMMERCIAL = $ninNOMCOMMERCIAL;

        return $this;
    }

    public function getNinFORMEUNITE(): ?string
    {
        return $this->ninFORMEUNITE;
    }

    public function setNinFORMEUNITE(?string $ninFORMEUNITE): self
    {
        $this->ninFORMEUNITE = $ninFORMEUNITE;

        return $this;
    }

    public function getNinSTATUT(): ?string
    {
        return $this->ninSTATUT;
    }

    public function setNinSTATUT(?string $ninSTATUT): self
    {
        $this->ninSTATUT = $ninSTATUT;

        return $this;
    }

    public function getNinTYPEDEDOCUMENT(): ?string
    {
        return $this->ninTYPEDEDOCUMENT;
    }

    public function setNinTYPEDEDOCUMENT(?string $ninTYPEDEDOCUMENT): self
    {
        //no shared
        $this->ninTYPEDEDOCUMENT = $ninTYPEDEDOCUMENT;

        return $this;
    }

    public function getNinNUMERODEDOCUMENT(): ?string
    {
        return $this->ninNUMERODEDOCUMENT;
    }

    public function setNinNUMERODEDOCUMENT(?string $ninNUMERODEDOCUMENT): self
    {
        $this->ninNUMERODEDOCUMENT = $ninNUMERODEDOCUMENT;

        return $this;
    }

    public function getNinDATEDUDOCUMENT(): ?string
    {
        return $this->ninDATEDUDOCUMENT;
    }

    public function setNinDATEDUDOCUMENT(?string $ninDATEDUDOCUMENT): self
    {
        $this->ninDATEDUDOCUMENT = $ninDATEDUDOCUMENT;

        return $this;
    }

    public function getNinCIVILITE(): ?string
    {
        return $this->ninCIVILITE;
    }

    public function setNinCIVILITE(?string $ninCIVILITE): self
    {
        $this->ninCIVILITE = $ninCIVILITE;

        return $this;
    }

    public function getNinSEXE(): ?string
    {
        return $this->ninSEXE;
    }

    public function setNinSEXE(?string $ninSEXE): self
    {
        $this->ninSEXE = $ninSEXE;

        return $this;
    }

    public function getNinDATEDENAISSANCE(): ?string
    {
        return $this->ninDATEDENAISSANCE;
    }

    public function setNinDATEDENAISSANCE(?string $ninDATEDENAISSANCE): self
    {
        $this->ninDATEDENAISSANCE = $ninDATEDENAISSANCE;

        return $this;
    }

    public function getNinLIEUDENAISSANCE(): ?string
    {
        return $this->ninLIEUDENAISSANCE;
    }

    public function setNinLIEUDENAISSANCE(?string $ninLIEUDENAISSANCE): self
    {
        $this->ninLIEUDENAISSANCE = $ninLIEUDENAISSANCE;

        return $this;
    }

    public function getNinEMAILENTREPRISE(): ?string
    {
        return $this->ninEMAILENTREPRISE;
    }

    public function setNinEMAILENTREPRISE(?string $ninEMAILENTREPRISE): self
    {
        $this->ninEMAILENTREPRISE = $ninEMAILENTREPRISE;

        return $this;
    }

    public function getNinSIGLE(): ?string
    {
        return $this->ninSIGLE;
    }

    public function setNinSIGLE(?string $ninSIGLE): self
    {
        $this->ninSIGLE = $ninSIGLE;

        return $this;
    }

    public function getNinTYPEVOIE(): ?string
    {
        return $this->ninTYPEVOIE;
    }

    public function setNinTYPEVOIE(?string $ninTYPEVOIE): self
    {
        //no share
        $this->ninTYPEVOIE = $ninTYPEVOIE;

        return $this;
    }

    public function getNinNUMEROVOIE(): ?string
    {
        return $this->ninNUMEROVOIE;
    }

    public function setNinNUMEROVOIE(?string $ninNUMEROVOIE): self
    {
        $this->ninNUMEROVOIE = $ninNUMEROVOIE;

        return $this;
    }

    public function getNinTELEPHONEENTREPRISE(): ?string
    {
        return $this->ninTELEPHONEENTREPRISE;
    }

    public function setNinTELEPHONEENTREPRISE(?string $ninTELEPHONEENTREPRISE): self
    {
        $this->ninTELEPHONEENTREPRISE = $ninTELEPHONEENTREPRISE;

        return $this;
    }

    public function getNinCAPITAL(): ?string
    {
        return $this->ninCAPITAL;
    }

    public function setNinCAPITAL(?string $ninCAPITAL): self
    {
        $this->ninCAPITAL = $ninCAPITAL;

        return $this;
    }

    public function getNinEFFECTIFPERMANENT(): ?string
    {
        return $this->ninEFFECTIFPERMANENT;
    }

    public function setNinEFFECTIFPERMANENT(?string $ninEFFECTIFPERMANENT): self
    {
        $this->ninEFFECTIFPERMANENT = $ninEFFECTIFPERMANENT;

        return $this;
    }

    public function getNinEFFECTIFPERMANENTFEMME(): ?string
    {
        return $this->ninEFFECTIFPERMANENTFEMME;
    }

    public function setNinEFFECTIFPERMANENTFEMME(?string $ninEFFECTIFPERMANENTFEMME): self
    {
        $this->ninEFFECTIFPERMANENTFEMME = $ninEFFECTIFPERMANENTFEMME;

        return $this;
    }

    public function getNinEFFECTIFTEMPORAIRE(): ?string
    {
        return $this->ninEFFECTIFTEMPORAIRE;
    }

    public function setNinEFFECTIFTEMPORAIRE(?string $ninEFFECTIFTEMPORAIRE): self
    {
        $this->ninEFFECTIFTEMPORAIRE = $ninEFFECTIFTEMPORAIRE;

        return $this;
    }

    public function getNinEFFECTIFTEMPORAIREFEMME(): ?string
    {
        return $this->ninEFFECTIFTEMPORAIREFEMME;
    }

    public function setNinEFFECTIFTEMPORAIREFEMME(string $ninEFFECTIFTEMPORAIREFEMME): self
    {
        $this->ninEFFECTIFTEMPORAIREFEMME = $ninEFFECTIFTEMPORAIREFEMME;

        return $this;
    }

    public function getNinCHIFFREAFFAIRE(): ?string
    {
        return $this->ninCHIFFREAFFAIRE;
    }

    public function setNinCHIFFREAFFAIRE(?string $ninCHIFFREAFFAIRE): self
    {
        $this->ninCHIFFREAFFAIRE = $ninCHIFFREAFFAIRE;

        return $this;
    }

    public function getNinSOURCEFINANCEMENT(): ?string
    {
        return $this->ninSOURCEFINANCEMENT;
    }

    public function setNinSOURCEFINANCEMENT(?string $ninSOURCEFINANCEMENT): self
    {
        // no shared
        $this->ninSOURCEFINANCEMENT = $ninSOURCEFINANCEMENT;

        return $this;
    }

    public function getNinMODALITEEXPLOITATION(): ?string
    {
        return $this->ninMODALITEEXPLOITATION;
    }

    public function setNinMODALITEEXPLOITATION(?string $ninMODALITEEXPLOITATION): self
    {
        //no shared
        $this->ninMODALITEEXPLOITATION = $ninMODALITEEXPLOITATION;

        return $this;
    }

    public function getNinACTIVITE(): ?string
    {
        return $this->ninACTIVITE;
    }

    public function setNinACTIVITE(?string $ninACTIVITE): self
    {
        $this->ninACTIVITE = $ninACTIVITE;

        return $this;
    }

    public function getNinPRODUIT(): ?string
    {
        return $this->ninPRODUIT;
    }

    public function setNinPRODUIT(?string $ninPRODUIT): self
    {
        $this->ninPRODUIT = $ninPRODUIT;

        return $this;
    }

    public function getNinVOIE(): ?string
    {
        return $this->ninVOIE;
    }

    public function setNinVOIE(?string $ninVOIE): self
    {
        $this->ninVOIE = $ninVOIE;

        return $this;
    }

    public function getNinANNEECA(): ?string
    {
        return $this->ninANNEECA;
    }

    public function setNinANNEECA(?string $ninANNEECA): self
    {
        $this->ninANNEECA = $ninANNEECA;

        return $this;
    }

    public function getNinDATEOPERATION(): ?\DateTimeInterface
    {
        return $this->ninDATEOPERATION;
    }

    public function setNinDATEOPERATION(?\DateTimeInterface $ninDATEOPERATION): self
    {
        $this->ninDATEOPERATION = $ninDATEOPERATION;

        return $this;
    }

    public function isNinISTRAITED(): ?bool
    {
        return $this->ninISTRAITED;
    }

    public function setNinISTRAITED(?bool $ninISTRAITED): self
    {
        $this->ninISTRAITED = $ninISTRAITED;

        return $this;
    }

    public function getNinDATETRAITEMENT(): ?\DateTimeInterface
    {
        return $this->ninDATETRAITEMENT;
    }

    public function setNinDATETRAITEMENT(?\DateTimeInterface $ninDATETRAITEMENT): self
    {
        $this->ninDATETRAITEMENT = $ninDATETRAITEMENT;

        return $this;
    }

    public function getNinLOCALITE(): ?string
    {
        return $this->ninLOCALITE;
    }

    public function setNinLOCALITE(?string $ninLOCALITE): self
    {
        $this->ninLOCALITE = $ninLOCALITE;

        return $this;
    }

    public function getNinVILLE(): ?string
    {
        return $this->ninVILLE;
    }

    public function setNinVILLE(?string $ninVILLE): self
    {
        $this->ninVILLE = $ninVILLE;

        return $this;
    }

    public function getNinBOITEPOSTALE(): ?string
    {
        return $this->ninBOITEPOSTALE;
    }

    public function setNinBOITEPOSTALE(?string $ninBOITEPOSTALE): self
    {
        $this->ninBOITEPOSTALE = $ninBOITEPOSTALE;

        return $this;
    }

    public function getNinADRESSE(): ?string
    {
        return $this->ninADRESSE;
    }

    public function setNinADRESSE(?string $ninADRESSE): self
    {
        $this->ninADRESSE = $ninADRESSE;

        return $this;
    }

    public function getNinNINEAMERE(): ?string
    {
        return $this->ninNINEAMERE;
    }

    public function setNinNINEAMERE(?string $ninNINEAMERE): self
    {
        $this->ninNINEAMERE = $ninNINEAMERE;

        return $this;
    }

    
    public function __toString()
    {
        return $this->ninNinea;
    }


}
