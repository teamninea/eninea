<?php

namespace App\Entity;

use App\Repository\PiecesjointesRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Slug;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=PiecesjointesRepository::class)
 * @Vich\Uploadable
 */
class Piecesjointes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ninNomfichier;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ninTaillefichier;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="ninea_proposition", fileNameProperty="ninNomfichier", size="ninTaillefichier")
     *
     * @var File
     */
    private $ninFile;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=NineaProposition::class, inversedBy="piecesJointes")
     */
    private $nineaProposition;


    public function __toString()
    {
        return $this->ninNomfichier;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNinNomfichier(): ?string
    {
        return $this->ninNomfichier;
    }

    public function setNinNomfichier(?string $ninNomfichier): self
    {
        $this->ninNomfichier = $ninNomfichier;

        return $this;
    }

    public function getNinTaillefichier(): ?string
    {
        return $this->ninTaillefichier;
    }

    public function setNinTaillefichier(?string $ninTaillefichier): self
    {
        $this->ninTaillefichier = $ninTaillefichier;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get nOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @return  File
     */ 
    public function getNinFile()
    {
        return $this->ninFile;
    }

    /**
     * Set nOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @param  File  $ninFile  NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     */ 
    public function setNinFile(?File $ninFile= null)
    {
        $this->ninFile = $ninFile;

        if (null !== $ninFile) {
            $this->updatedAt = new DateTimeImmutable();
        }

        #return $this;
    }

    public function getNineaProposition(): ?NineaProposition
    {
        return $this->nineaProposition;
    }

    public function setNineaProposition(?NineaProposition $nineaProposition): self
    {
        $this->nineaProposition = $nineaProposition;

        return $this;
    }
}
