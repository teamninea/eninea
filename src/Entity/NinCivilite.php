<?php

namespace App\Entity;

use App\Repository\NinCiviliteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinCiviliteRepository::class)
 */
class NinCivilite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=NinPersonne::class, mappedBy="ninCivilite")
     */
    private $ninPersonnes;

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->ninPersonnes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, NinPersonne>
     */
    public function getNinPersonnes(): Collection
    {
        return $this->ninPersonnes;
    }

    public function addNinPersonne(NinPersonne $ninPersonne): self
    {
        if (!$this->ninPersonnes->contains($ninPersonne)) {
            $this->ninPersonnes[] = $ninPersonne;
            $ninPersonne->setNinCivilite($this);
        }

        return $this;
    }

    public function removeNinPersonne(NinPersonne $ninPersonne): self
    {
        if ($this->ninPersonnes->removeElement($ninPersonne)) {
            // set the owning side to null (unless already changed)
            if ($ninPersonne->getNinCivilite() === $this) {
                $ninPersonne->setNinCivilite(null);
            }
        }

        return $this;
    }
}
