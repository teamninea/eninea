<?php

namespace App\Entity;

use App\Repository\NinPaysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinPaysRepository::class)
 */
class NinPays
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=NinPersonne::class, mappedBy="ninNationalite")
     */
    private $ninPersonnes;

    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->ninPersonnes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, NinPersonne>
     */
    public function getNinPersonnes(): Collection
    {
        return $this->ninPersonnes;
    }

    public function addNinPersonne(NinPersonne $ninPersonne): self
    {
        if (!$this->ninPersonnes->contains($ninPersonne)) {
            $this->ninPersonnes[] = $ninPersonne;
            $ninPersonne->setNinNationalite($this);
        }

        return $this;
    }

    public function removeNinPersonne(NinPersonne $ninPersonne): self
    {
        if ($this->ninPersonnes->removeElement($ninPersonne)) {
            // set the owning side to null (unless already changed)
            if ($ninPersonne->getNinNationalite() === $this) {
                $ninPersonne->setNinNationalite(null);
            }
        }

        return $this;
    }
}
