<?php

namespace App\Entity;

use App\Repository\RefCACRRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RefCACRRepository::class)
 */
class RefCACR
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * 
     */
    private $codeCacr;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity=RefCAV::class, inversedBy="refCACRs")
     */
    private $ninCAV;

    /**
     * @ORM\OneToMany(targetEntity=QVH::class, mappedBy="ninCACR")
     */
    private $qVHs;



    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $cacrActiveF;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cacrCDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cacrCUSER;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cacrCDMIG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cacrMDATE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cacrMUSER;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninCacr")
     */
    private $nineaPropositions;


    public function __toString(): string
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->qVHs = new ArrayCollection();
        $this->nineaPropositions = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNinCAV(): ?RefCAV
    {
        return $this->ninCAV;
    }

    public function setNinCAV(?RefCAV $ninCAV): self
    {
        $this->ninCAV = $ninCAV;

        return $this;
    }

    /**
     * @return Collection<int, QVH>
     */
    public function getQVHs(): Collection
    {
        return $this->qVHs;
    }

    public function addQVH(QVH $qVH): self
    {
        if (!$this->qVHs->contains($qVH)) {
            $this->qVHs[] = $qVH;
            $qVH->setNinCACR($this);
        }

        return $this;
    }

    public function removeQVH(QVH $qVH): self
    {
        if ($this->qVHs->removeElement($qVH)) {
            // set the owning side to null (unless already changed)
            if ($qVH->getNinCACR() === $this) {
                $qVH->setNinCACR(null);
            }
        }

        return $this;
    }

    public function getCodeCacr(): ?string
    {
        return $this->codeCacr;
    }

    public function setCodeCacr(?string $codeCacr): self
    {
        $this->codeCacr = $codeCacr;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCacrActiveF(): ?string
    {
        return $this->cacrActiveF;
    }

    public function setCacrActiveF(?string $cacrActiveF): self
    {
        $this->cacrActiveF = $cacrActiveF;

        return $this;
    }

    public function getCacrCDATE(): ?string
    {
        return $this->cacrCDATE;
    }

    public function setCacrCDATE(?string $cacrCDATE): self
    {
        $this->cacrCDATE = $cacrCDATE;

        return $this;
    }

    public function getCacrCUSER(): ?string
    {
        return $this->cacrCUSER;
    }

    public function setCacrCUSER(?string $cacrCUSER): self
    {
        $this->cacrCUSER = $cacrCUSER;

        return $this;
    }

    public function getCacrCDMIG(): ?string
    {
        return $this->cacrCDMIG;
    }

    public function setCacrCDMIG(?string $cacrCDMIG): self
    {
        $this->cacrCDMIG = $cacrCDMIG;

        return $this;
    }

    public function getCacrMDATE(): ?string
    {
        return $this->cacrMDATE;
    }

    public function setCacrMDATE(?string $cacrMDATE): self
    {
        $this->cacrMDATE = $cacrMDATE;

        return $this;
    }

    public function getCacrMUSER(): ?string
    {
        return $this->cacrMUSER;
    }

    public function setCacrMUSER(?string $cacrMUSER): self
    {
        $this->cacrMUSER = $cacrMUSER;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaPropositions(): Collection
    {
        return $this->nineaPropositions;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaPropositions->contains($nineaProposition)) {
            $this->nineaPropositions[] = $nineaProposition;
            $nineaProposition->setNinCacr($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaPropositions->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinCacr() === $this) {
                $nineaProposition->setNinCacr(null);
            }
        }

        return $this;
    }
}
