<?php

namespace App\Entity;

use App\Repository\NinStatutRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NinStatutRepository::class)
 */
class NinStatut
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=NineaProposition::class, mappedBy="ninStatut")
     */
    private $nineaPropositions;

    public function __toString()
    {
        return $this->libelle;
    }

    public function __construct()
    {
        $this->nineaPropositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection<int, NineaProposition>
     */
    public function getNineaPropositions(): Collection
    {
        return $this->nineaPropositions;
    }

    public function addNineaProposition(NineaProposition $nineaProposition): self
    {
        if (!$this->nineaPropositions->contains($nineaProposition)) {
            $this->nineaPropositions[] = $nineaProposition;
            $nineaProposition->setNinStatut($this);
        }

        return $this;
    }

    public function removeNineaProposition(NineaProposition $nineaProposition): self
    {
        if ($this->nineaPropositions->removeElement($nineaProposition)) {
            // set the owning side to null (unless already changed)
            if ($nineaProposition->getNinStatut() === $this) {
                $nineaProposition->setNinStatut(null);
            }
        }

        return $this;
    }
}
