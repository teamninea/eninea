<?php

namespace App\Form;

use App\Entity\NinPersonne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NinPersonne1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('ninNom', TextType::class, [
                'required'=>true,
                'attr' => array('class' => 'form-control form-control-sm ' )
            ])
            ->add('ninPrenom', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninDatenaissance', DateType::class, [
                'widget'=>'single_text',
                'required'=>true,
                'attr' => array('class' => 'form-control form-control-sm w-100 js-datepicker' )
            ])
            ->add('ninLieunaissance', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninCni', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninDatecni', DateType::class, [
                'widget'=>'single_text',
                'required'=>true,
                'attr' => array('class' => 'form-control form-control-sm js-datepicker' )
            ])
            ->add('ninTelephone', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninEmail', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninAdresse', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninVoie', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " ",
                'required'=>false
            ])
            ->add('ninNumerovoie', TextType::class, [
                'required' => false,
                'attr' => ['class' => 'form-control form-control-sm']
            ])
            ->add('ninSexe')
            ->add('ninCivilite')
            ->add('ninNationalite')
            ->add('ninTypevoie')
            ->add('ninQVH', null, [
                'placeholder' => 'Seletionner ....',
                'required'=>false, 
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NinPersonne::class,
        ]);
    }
}
