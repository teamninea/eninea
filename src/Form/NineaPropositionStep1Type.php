<?php

namespace App\Form;

use App\Entity\NiFormejuridique;
use App\Entity\NinFormeunite;
use App\Entity\NinTypedocuments;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NineaPropositionStep1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        # $validValues = [2, 4];
		/*$validValues = [2, 4];
		$builder->add('numberOfWheels', ChoiceType::class, [
			'choices' => array_combine($validValues, $validValues),
            'choices' => array_combine($validDoctypeValues, $validDoctypeValues),
			'placeholder' => '',
		]); temoin */  

        $validDoctypeValues = [2, 4];
        $builder
            ->add('ninFormeunite', EntityType::class, [
                'class' => NinFormeunite::class,
                'choice_label' => 'libelle',
                'placeholder' => '',
            ])
            ->add('formeJuridique', EntityType::class, [
                'class' => NiFormejuridique::class,
                'choice_label' => 'libelle',
                'placeholder' => '',
            ])
            ->add('ninSigle', TextType::class, [
                'attr' => ['class' => 'form-control form-control-sm'],
                'placeholder' => '',
            ])
            ->add('ninEnseigne', TextType::class, [
                'attr' => ['class' => 'form-control form-control-sm'],
                'placeholder' => '',
            ])
            ->add('ninNomcommercial', TextType::class, [
                'attr' => ['class' => 'form-control form-control-sm'],
                'placeholder' => '',
            ])
            ->add('ninTypedocuments', EntityType::class, [
                'class' => NinTypedocuments::class,
                'choice_label' => 'libelle',
                'placeholder' => '',
            ])
            ->add('ninNumerodocument', TextType::class, [
                'attr' => ['class' => 'form-control form-control-sm'],
                'placeholder' => '',
            ])
            ->add('ninDatedocument', DateType::class, [
                'attr' => ['class' => 'form-control form-control-sm'],
                'placeholder' => '',
            ])
        ;
    }



	public function getBlockPrefix() {
		return 'createNineaPropositionStep1';
	}


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
