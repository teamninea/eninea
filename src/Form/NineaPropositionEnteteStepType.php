<?php

namespace App\Form;

use App\Entity\NiFormejuridique;
use App\Entity\NineaProposition;
use App\Entity\NinFormeunite;
use App\Entity\NinPersonne;
use App\Entity\NinStatut;
use App\Entity\NinTypedocuments;
use App\Entity\RefCACR;
use App\Entity\RefCAV;
use App\Entity\RefDepartement;
use App\Entity\RefRegion;
use App\Repository\RefCAVRepository;
use App\Repository\RefDepartementRepository;
use App\Repository\RefRegionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Factory\Cache\ChoiceLabel;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class NineaPropositionEnteteStepType extends AbstractType
{

    private $countryRepositoryReg;
    private $stateRepositoryDep;
    private $stateCitiesRepositoryCav;

    public function __construct(
        RefRegionRepository $countryRepositoryReg,
        RefDepartementRepository $stateRepositoryDep,
        RefCAVRepository $stateCitiesRepositoryCav
        ) {
        $this->countryRepositoryReg = $countryRepositoryReg;
        $this->stateRepositoryDep = $stateRepositoryDep;
        $this->stateCitiesRepositoryCav = $stateCitiesRepositoryCav;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {


        $builder

        
        /*->add('formeJuridique', EntityType::class, [
            'class' => NiFormejuridique::class, 
            'choice_label' => 'libelle',
            'placeholder' => 'Seletionner ....', 
            "mapped" => false,
            "attr" => array("" =>'', 'id'=>'formeJuridiqueID2'),
            'required' => true,
            ])*/

            //
            ->add('ninRegion', EntityType::class, [
                'class' => RefRegion::class,
                'label' => ' ',
                'required' => true,
                'choice_label' => function(RefRegion $regi) {
                    return $regi->getLibelle();
                },
                'placeholder' => 'Seletionner ....', 
            ])
            ->add('ninDepartement', EntityType::class, [
                'class' => RefDepartement::class,
                'label' => ' ',
                'required' => true,
                'choice_label' => function(RefDepartement $regi) {
                    return $regi->getLibelle();
                },
                'placeholder' => 'Seletionner ....', 
            ])
                
            
            ->add('ninCommune', EntityType::class, [
                'class' => RefCAV::class, 
                'choice_label' => 'libelle',
                'placeholder' => 'Seletionner ....', 
                "mapped" => false,
                'required'=>false,
                ])
                        
            ->add('ninCacr', EntityType::class, [
                'class' => RefCACR::class, 
                'choice_label' => 'libelle',
                'placeholder' => 'Seletionner ....', 
                "mapped" => false,
                'required'=>false,
                ])
                            
            /*->add('formeUnite', EntityType::class, [
                'class' => NinFormeunite::class, 
                'choice_label' => 'libelle',
                'placeholder' => 'Seletionner ....', 
                "mapped" => false,
                'required' => true,
            ])
            
            ->add('ninSigle')
            ->add('ninEnseigne')
            ->add('ninNomcommercial')
            
            ->add('ninNumerodocument')
            
            ->add('ninDatedocument', DateType::class, [
                'widget'=>'single_text',
                'required'=>true,
                'attr' => array('class' => 'form-control form-control-sm js-datepicker' )
            ])
                
            ->add('ninTypedocuments', EntityType::class, [
                'class' => NinTypedocuments::class,
            'choice_label' => 'libelle',
            'placeholder' =>'Seletionner ....',
            'placeholder' =>'Seletionner ....',
            'attr' => array('class' => 'form-control form-select form-select-sm', ),
            ])
            ->add('ninStatut', EntityType::class, [
                'class' => NinStatut::class,
                'choice_label' => 'libelle',
                'placeholder' =>'Seletionner ....',
                'attr' => array('class' => 'form-control form-select form-select-sm'),
                    
            ])*/

            ->add('brochurecni', FileType::class, [
                'label' => ' ',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])

            ->add('brochurerccm', FileType::class, [
                'label' => ' ',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])


            ->add(                
                // nested form here
                $builder->create(
                    'ninPersonne', 
                    NinPersonne1Type::class, 
                    array('by_reference' => true ), // adds ORM capabilities
                    
            ))

            ->add(
                $builder->create(
                    'ninCoordonnees',
                    CollectionType::class,
                    array('entry_type' => NinCoordonneesType::class,
                          'entry_options' => ['label' => false, 'attr' => array('class' => 'form-control form-control-sm form-select form-select-sm select2')],
                          'allow_add' => true, // si je veux ajouter +eurs adresses dynamic
                          'label' => ' ',
                          'by_reference' => false,
                          'allow_delete'=>true,
                    ),
                )
            )
        ;

        
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NineaProposition::class,
        ]);
    }

}
