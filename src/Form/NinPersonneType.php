<?php

namespace App\Form;

use App\Entity\NinPersonne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NinPersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ninRaisonsociale')
            ->add('ninSigle')
            ->add('ninNom')
            ->add('ninPrenom')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NinPersonne::class,
        ]);
    }
}
