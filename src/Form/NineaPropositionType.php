<?php

namespace App\Form;

use App\Entity\NineaProposition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NineaPropositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ninSigle')
            ->add('ninEnseigne')
            ->add('ninNomcommercial')
            ->add('ninActiviteglobale')
            ->add('ninNumerodocument')
            ->add('ninDatedocument')
            //->add('ninPersonne')
            ->add('ninTypedocuments')
            ->add('ninStatut')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NineaProposition::class,
        ]);
    }
}
