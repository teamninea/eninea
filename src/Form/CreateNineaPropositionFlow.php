<?php

namespace App\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use App\Form\NineaPropositionStep2Type;
use App\Form\NineaPropositionStep1Type;

class CreateNineaPropositionFlow extends FormFlow {

	protected function loadStepsConfig() {
		return [
			[
				'label' => 'En tête',
				'form_type' => NineaPropositionStep1Type::class,
			],
			[
				'label' => 'engine',
				'form_type' => NineaPropositionStep2Type::class,
				'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
					return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->canHaveEngine();
				},
			],
			[
				'label' => 'confirmation',
			],
		];
	}
    
}