<?php

namespace App\Form;

use App\Entity\NinCoordonnees;
use App\Entity\NinTypevoie;
use App\Entity\QVH;
use App\Entity\RefCACR;
use App\Entity\RefCAV;
use App\Entity\RefDepartement;
use App\Entity\RefRegion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NinCoordonneesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ninNumerovoie', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninVoie', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninAdresse1', TextType::class, [
                'attr'=>['class'=>'form-control form-control-sm']
            ])
            ->add('ninEmail', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninTelephone1', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninTelephone2', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninBp', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninUrl', TextType::class, [
                'attr'=>['class'=> 'form-control form-control-sm'],
                'label' => " "])
            ->add('ninQvh',EntityType::class,array(

                'placeholder'=>'Sélectionner.......',
                'class'=>QVH::class,
                'choice_label'=>'libelle',
                'attr'=>array('class'=>'form-select form-select-sm select2 form-control'),
                'required'=>false,
                'disabled'=>false,
                'label'=>"Quartier"


            ))
            ->add('ninTypevoie',EntityType::class,array(

                'placeholder'=>'Sélectionner.......',
                'class'=>NinTypevoie::class,
                'choice_label'=>'libelle',
                'attr'=>array('class'=>' form-select form-select-sm select2 form-control'),
                'required'=>false,
                'disabled'=>false,
                'label'=>"Type voie"


            ))


            ->add('adrRegion', EntityType::class, [
                'class' => RefRegion::class,
                'label' => ' ',
                "mapped" => false,
                'required' => true,
                'choice_label' => function(RefRegion $regi) {
                    return $regi->getLibelle();
                },
                'placeholder' => 'Seletionner ....', 
            ])
            ->add('adrDepartement', EntityType::class, [
                'class' => RefDepartement::class,
                'label' => ' ',
                "mapped" => false,
                'required' => true,
                'choice_label' => function(RefDepartement $regi) {
                    return $regi->getLibelle();
                },
                'placeholder' => 'Seletionner ....', 
            ])
                
            
            ->add('adrCommune', EntityType::class, [
                'class' => RefCAV::class, 
                'choice_label' => 'libelle',
                'placeholder' => 'Seletionner ....', 
                "mapped" => false,
                'required'=>false,
                ])
                        
            ->add('adrCacr', EntityType::class, [
                'class' => RefCACR::class, 
                'choice_label' => 'libelle',
                'placeholder' => 'Seletionner ....', 
                "mapped" => false,
                'required'=>false,
                ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NinCoordonnees::class,
            //'data_class' => null ,
        ]);
    }
}
