<?php

namespace App\Controller;

use App\Entity\NinCoordonnees;
use App\Entity\NineaProposition;
use App\Form\CreateNineaPropositionFlow;
use App\Form\NineaPropositionEnteteStepType;
use App\Form\NineaPropositionType;
use App\Repository\NineaPropositionRepository;
use Craue\FormFlowBundle\Form\FormFlow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ninea/proposition")
 */
class NineaPropositionController extends AbstractController
{
    /**
     * @Route("/", name="app_ninea_proposition_index", methods={"GET"})
     */
    public function index(NineaPropositionRepository $nineaPropositionRepository): Response
    {
        return $this->render('ninea_proposition/index.html.twig', [
            'ninea_propositions' => $nineaPropositionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_ninea_proposition_new", methods={"GET", "POST"})
     */
    public function new(Request $request, NineaPropositionRepository $nineaPropositionRepository, EntityManagerInterface $em, $toUpdateId=0): Response
    {

        if ($toUpdateId != 0)
            $nineaProposition = $this->getDoctrine()->getRepository(NineaProposition::class)->find($toUpdateId);
        else
            $nineaProposition = new NineaProposition();
            //$nineaProposition = new NineaProposition();
 
        $ninCoordonnees = new ArrayCollection();
        foreach ($nineaProposition->getNinCoordonnees() as $adresse) {
            $ninCoordonnees->add($adresse);
        }
        
        $form = $this->createForm(NineaPropositionEnteteStepType::class, $nineaProposition, []);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $adresse = new NinCoordonnees();

            if (!$form['ninCoordonnees']->getData()) {
                # TODO do something her
            }else {
                //$adresse->setNinAdresse1($form['ninAdresse1']->getData());

                //dd($form['ninCoordonnees']->getData());
            }
            $nineaPropositionRepository->add($nineaProposition, true);
            
            //dd($nineaProposition);
            return $this->redirectToRoute('app_ninea_proposition_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ninea_proposition/new.html.twig', [
            'ninea_proposition' => $nineaProposition,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_ninea_proposition_show", methods={"GET"})
     */
    public function show(NineaProposition $nineaProposition): Response
    {
        return $this->render('ninea_proposition/show.html.twig', [
            'ninea_proposition' => $nineaProposition,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_ninea_proposition_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, NineaProposition $nineaProposition, NineaPropositionRepository $nineaPropositionRepository): Response
    {
        $form = $this->createForm(NineaPropositionType::class, $nineaProposition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nineaPropositionRepository->add($nineaProposition, true);

            return $this->redirectToRoute('app_ninea_proposition_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ninea_proposition/edit.html.twig', [
            'ninea_proposition' => $nineaProposition,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_ninea_proposition_delete", methods={"POST"})
     */
    public function delete(Request $request, NineaProposition $nineaProposition, NineaPropositionRepository $nineaPropositionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$nineaProposition->getId(), $request->request->get('_token'))) {
            $nineaPropositionRepository->remove($nineaProposition, true);
        }

        return $this->redirectToRoute('app_ninea_proposition_index', [], Response::HTTP_SEE_OTHER);
    }


    public function createVehicleAction() {
        $formData = new NineaProposition(); // Your form data class. Has to be an object, won't work properly with an array.

        # dd('create form flow in symfony !');
        
        # $flow = $this->get('form.flow.createNineaProposition'); // must match the flow's service id

        $flow = new CreateNineaPropositionFlow();
        $flow->bind($formData);
        dd($flow);


        // form of the current step
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                // flow finished
                $em = $this->getDoctrine()->getManager();
                $em->persist($formData);
                $em->flush();

                $flow->reset(); // remove step data from the session

                return $this->redirectToRoute('app_ninea_proposition_new'); // redirect when done
            }
        }

        return $this->render('ninea_proposition/createNineaProposition.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }


    /**
     * Commented form : action pour gerer le formulaire de l entete
     * @Route("/{id}/steps", name="app_entete_step_form", methods={"POST"})
     */
    public function enteteStepForm(Request $request): Response
    {
        dd('const');

        return $this->render("ninea_proposition/_enteteStepForm.html.twig", [
        ]);
    }
}
