<?php

namespace App\Controller;

use App\Entity\NinPartage;
use App\Form\SearchAnnuaireType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/search/annuaire")
 */
class SearchAnnuaireController extends AbstractController
{
    /**
     * @Route("/", name="app_search_annuaire", methods={"GET", "POST"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $form = $this->createForm(SearchAnnuaireType::class);


        $form->handleRequest($request);

        // initialiser une session 
        $session = new Session();
        $query = "";
        
        $limits = 4;
        
        if ($request->isMethod('POST')) {

            if ($form->isSubmitted() && $form->isValid()) {
                $query = $form->get('query')->getData();
                
                $entreprise = $this->getDoctrine()->getRepository(NinPartage::class)->findBy(["ninNinea"=>$query]); //reste à crrer cette methode search dans repository ou entity
                
                $session->set('dql', $query);
                
                // $articles = $paginator->paginate(
                    //     $entreprise, 
                    //     $request->query->getInt('page', 1), 
                    //     //$this->get('request')->query->getInt('page', 1),
                    //     $limits,
                    // );
                    
                $articles = $entreprise;
    
                return $this->render('search_annuaire/_layout_results.html.twig', [
                    'query' => $query,
                    'articles' => $articles,
                    'limits' => $limits,
                    'form' => $form->createView()
                ]);
            }
        }

        // ceci n'est util que quand on utilise le paginator de doctrine
        if($request->isMethod('GET') && $request->query->get('page')){
            $entreprise = $this->getDoctrine()->getRepository(NinPartage::class)->findBySearchTerms($session->get('dql')); //reste à crrer cette methode search dans repository ou entity
            
            $articles = $paginator->paginate(
                $entreprise, 
                $request->query->get('page'), 
                //$this->get('request')->query->getInt('page', 1),
                $limits,
            );

            return $this->render('search_annuaire/_layout_results.html.twig', [
                'query' => $session->get('dql'),
                'articles' => $articles,
                'limits' => $limits,
                'form' => $form->createView()
            ]);

            
        }

        return $this->render('search_annuaire/index.html.twig', [
            'controller_name' => 'SearchAnnuaireController',
            'form' => $form->createView()

        ]);
    }


    /**
     * Cette action correspond à la recherche dans annuaire des entreprises 
     * On applique la recherche avec LIKE pour avoir beaucoup de resultasts
     * @Route("/numNinea", name="app_detail", methods={"GET", "POST"})
     */
    public function detail(Request $request, PaginatorInterface $paginator, $numNinea=""): Response
    {

        if ($numNinea != "") {
            if ($request->isXMLHttpRequest()) {

                
                //TODO ici requete en base pour recuperer les details de l'entreprise
                $ficheEnter  = $this->getDoctrine()->getRepository(NinPartage::class)->findBy(array("ninNinea"=>$numNinea));
    
                //dd($ficheEnter);
                
                return $this->json($ficheEnter);
            }

        }

        $form = $this->createForm(SearchAnnuaireType::class);

        $form->handleRequest($request);

        // initialiser une session 
        $session = new Session();

        $limits = 4;
        $query = "";

        if ($form->isSubmitted() && $form->isValid()) {
            $query = $form->get('query')->getData();

            // $entreprise = $this->getDoctrine()->getRepository(NinPartage::class)->search($query); //reste à crrer cette methode search dans repository ou entity
            $entreprise = $this->getDoctrine()->getRepository(NinPartage::class)->findBySearchTerms($query); //reste à crrer cette methode search dans repository ou entity

            $session->set('dql', $query);
                
            $articles = $paginator->paginate(
                    $entreprise, 
                    $request->query->getInt('page', 1), 
                    //$this->get('request')->query->getInt('page', 1),
                    $limits,
                );
                
            $articles = $entreprise;


            return $this->render('search_annuaire/results.html.twig', [
                'query' => $query,
                'articles' => $articles,
                'form' => $form->createView(),
                'limits' => $limits,
            ]);
        }

        return $this->render('search_annuaire/form.html.twig', [
            'form' => $form->createView(),
        ]);

    }


    /**
     * Cette route pas encore utilise
     * @Route("/searchs", name="app_searchs")
     */
    public function searchs(Request $request, PaginatorInterface $paginator): Response
    {
        $form = $this->createForm(SearchAnnuaireType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $query = $form->get('query')->getData();

            $entreprise = $this->getDoctrine()->getRepository(NinPartage::class)->search($query); //reste à crrer cette methode search dans repository ou entity

            return $this->render('search_annuaire/results.html.twig', [
                'query' => $query,
                'articles' => $entreprise,
            ]);
        }

        return $this->render('search_annuaire/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
