<?php

namespace App\Controller;

use App\Entity\NinCoordonnees;
use App\Form\NinCoordonneesType;
use App\Repository\NinCoordonneesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nin/coordonnees")
 */
class NinCoordonneesController extends AbstractController
{
    /**
     * @Route("/", name="app_nin_coordonnees_index", methods={"GET"})
     */
    public function index(NinCoordonneesRepository $ninCoordonneesRepository): Response
    {
        return $this->render('nin_coordonnees/index.html.twig', [
            'nin_coordonnees' => $ninCoordonneesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_nin_coordonnees_new", methods={"GET", "POST"})
     */
    public function new(Request $request, NinCoordonneesRepository $ninCoordonneesRepository): Response
    {
        $ninCoordonnee = new NinCoordonnees();
        $form = $this->createForm(NinCoordonneesType::class, $ninCoordonnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ninCoordonneesRepository->add($ninCoordonnee, true);

            return $this->redirectToRoute('app_nin_coordonnees_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nin_coordonnees/new.html.twig', [
            'nin_coordonnee' => $ninCoordonnee,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_nin_coordonnees_show", methods={"GET"})
     */
    public function show(NinCoordonnees $ninCoordonnee): Response
    {
        return $this->render('nin_coordonnees/show.html.twig', [
            'nin_coordonnee' => $ninCoordonnee,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_nin_coordonnees_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, NinCoordonnees $ninCoordonnee, NinCoordonneesRepository $ninCoordonneesRepository): Response
    {
        $form = $this->createForm(NinCoordonneesType::class, $ninCoordonnee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ninCoordonneesRepository->add($ninCoordonnee, true);

            return $this->redirectToRoute('app_nin_coordonnees_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nin_coordonnees/edit.html.twig', [
            'nin_coordonnee' => $ninCoordonnee,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_nin_coordonnees_delete", methods={"POST"})
     */
    public function delete(Request $request, NinCoordonnees $ninCoordonnee, NinCoordonneesRepository $ninCoordonneesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ninCoordonnee->getId(), $request->request->get('_token'))) {
            $ninCoordonneesRepository->remove($ninCoordonnee, true);
        }

        return $this->redirectToRoute('app_nin_coordonnees_index', [], Response::HTTP_SEE_OTHER);
    }
}
