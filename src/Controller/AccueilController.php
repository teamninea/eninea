<?php

namespace App\Controller;

use App\Entity\NinPartage;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/accueil", name="app_accueil")
     */
    public function index(): Response
    {

        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }


        
    /**
     * @Route("/accueil/nineaConnect", name="ninea_connect", methods={"GET"})
     */
    public function ninea_connect(Request $request,EntityManagerInterface $entityManager): Response
    {
      

        return $this->render('accueil/net_accueil.html.twig',[
            
        ]);
    }


    /**
     * @Route("/accueil/procedure", name="app_ninea_proc", methods={"GET", "POST"})
     */
    public function ninea_procedure(Request $request,EntityManagerInterface $entityManager): Response
    {
        return $this->render('accueil/_procedure.html.twig',[
            
        ]);
    }

    /**
     * @Route("/accueil/fu", name="app_ninea_fu", methods={"GET", "POST"})
     */
    public function ninea_fu(Request $request,EntityManagerInterface $entityManager): Response
    {
        return $this->render('accueil/_fu_layout.html.twig',[
            
        ]);
    }

    /**
     * @Route("/accueil/pmorale", name="app_ninea_pmorale", methods={"GET", "POST"})
     */
    public function ninea_pmorale(Request $request,EntityManagerInterface $entityManager): Response
    {
        return $this->render('accueil/_pmorale_layout.html.twig',[
            
        ]);
    }


    /**
     * @Route("/accueil/userlistdemande", name="app_list_demande", methods={"GET", "POST"})
     */
    public function ninea_listdemandes(Request $request,EntityManagerInterface $entityManager): Response
    {
        return $this->render('accueil/_userlistdemande_layout.html.twig',[
            
        ]);
    }




    

}
