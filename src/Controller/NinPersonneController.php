<?php

namespace App\Controller;

use App\Entity\NinPersonne;
use App\Form\NinPersonne1Type;
use App\Repository\NinPersonneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nin/personne")
 */
class NinPersonneController extends AbstractController
{
    /**
     * @Route("/", name="app_nin_personne_index", methods={"GET"})
     */
    public function index(NinPersonneRepository $ninPersonneRepository): Response
    {
        return $this->render('nin_personne/index.html.twig', [
            'nin_personnes' => $ninPersonneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_nin_personne_new", methods={"GET", "POST"})
     */
    public function new(Request $request, NinPersonneRepository $ninPersonneRepository): Response
    {
        $ninPersonne = new NinPersonne();
        $form = $this->createForm(NinPersonne1Type::class, $ninPersonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ninPersonneRepository->add($ninPersonne, true);

            return $this->redirectToRoute('app_nin_personne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nin_personne/new.html.twig', [
            'nin_personne' => $ninPersonne,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_nin_personne_show", methods={"GET"})
     */
    public function show(NinPersonne $ninPersonne): Response
    {
        return $this->render('nin_personne/show.html.twig', [
            'nin_personne' => $ninPersonne,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_nin_personne_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, NinPersonne $ninPersonne, NinPersonneRepository $ninPersonneRepository): Response
    {
        $form = $this->createForm(NinPersonne1Type::class, $ninPersonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ninPersonneRepository->add($ninPersonne, true);

            return $this->redirectToRoute('app_nin_personne_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('nin_personne/edit.html.twig', [
            'nin_personne' => $ninPersonne,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_nin_personne_delete", methods={"POST"})
     */
    public function delete(Request $request, NinPersonne $ninPersonne, NinPersonneRepository $ninPersonneRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ninPersonne->getId(), $request->request->get('_token'))) {
            $ninPersonneRepository->remove($ninPersonne, true);
        }

        return $this->redirectToRoute('app_nin_personne_index', [], Response::HTTP_SEE_OTHER);
    }
}
